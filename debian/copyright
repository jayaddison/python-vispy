Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: vispy
Source: https://github.com/vispy/vispy
Upstream-Contact: vispy@googlegroups.com

Files: *
Copyright: 2013-2019 VisPy Development Team
License: BSD-3-Clause

Files: debian/*
Copyright: 2016 Ghislain Antony Vaillant <ghisvail@gmail.com>
License: BSD-3-Clause

Files: examples/*
Copyright: 2013-2019 VisPy Development Team
License: PublicDomain
 The examples code in the examples directory can be considered public
 domain, unless otherwise indicated in the corresponding source file.

Files: examples/gloo/geometry_shader.py
 examples/gloo/gpuimage.py
 examples/gloo/post_processing.py
 examples/plotting/*
 examples/basics/scene/*
 examples/basics/visuals/*
 examples/benchmark/*
 examples/collections/tiger.py
 examples/demo/gloo/*
 examples/demo/gloo/jfa/jfa_translation.py
 examples/demo/plot/plot.py
 examples/demo/scene/*
 examples/demo/visuals/wiggly_bar.py
 examples/jupyter/Rotating?Cube.ipynb
 examples/offscreen/*
 examples/tutorial/*
Copyright: 2013-2019 VisPy Development Team
License: BSD-3-Clause

Files: examples/plotting/scatter_histogram.py
 examples/basics/visuals/grid_mesh.py
 examples/basics/visuals/line_plot3d.py
 examples/basics/visuals/tube.py
 examples/demo/gloo/boids.py
 examples/demo/gloo/cloud.py
 examples/demo/gloo/fireworks.py
 examples/demo/gloo/glsl_sandbox_cube.py
 examples/demo/gloo/graph.py
 examples/demo/gloo/imshow_cuts.py
 examples/demo/gloo/jfa/fragment_display.glsl
 examples/demo/gloo/jfa/fragment_flood.glsl
 examples/demo/gloo/jfa/fragment_seed.glsl
 examples/demo/gloo/jfa/jfa_vispy.py
 examples/demo/gloo/jfa/vertex.glsl
 examples/demo/gloo/jfa/vertex_vispy.glsl
 examples/demo/gloo/ndscatter.py
 examples/demo/gloo/two_qt_widgets.py
 examples/demo/gloo/voronoi.py
 examples/demo/gloo/galaxy/*
 examples/demo/scene/picking.py
Copyright: 2013-2019 VisPy Development Team
License: PublicDomain
 The examples code in the examples directory can be considered public
 domain, unless otherwise indicated in the corresponding source file.

Files: vispy/ext/cubehelix.py
Copyright: 2014 James R. A. Davenport and contributors
License: BSD-2-Clause

Files: vispy/ext/cocoapy.py
Copyright: 2006-2008 Alex Holkner
License: BSD-3-Clause
Comment: https://github.com/named-data/PyNDN2/blob/master/python/pyndn/contrib/cocoapy/LICENSE

Files: vispy/ext/fontconfig.py
Copyright: 2008-2023, pyglet contributors
           2013-2019 VisPy Development Team
License: BSD-3-Clause

Files: vispy/geometry/normals.py
 vispy/geometry/parametric.py
 vispy/gloo/tests/test_buffer.py
 vispy/gloo/tests/test_globject.py
 vispy/gloo/tests/test_program.py
 vispy/gloo/tests/test_texture.py
 vispy/gloo/tests/test_use_gloo.py
 vispy/gloo/tests/test_wrappers.py
 vispy/visuals/collections/*
 vispy/visuals/glsl/*
Copyright: 2014 Nicolas P. Rougier
License: BSD-3-Clause

Files: vispy/glsl/build_spatial_filters.py
Copyright: 2009-2011  Nicolas P. Rougier
License: BSD-2-Clause

Files: vispy/util/svg/*
Copyright: 2013 Nicolas P. Rougier
License: BSD-2-Clause

Files: vispy/visuals/text/_sdf_gpu.py
Copyright: 2014 Eric Larson <larson.eric.d@gmail.com>
License: BSD-3-Clause

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: zlib
 This software is provided 'as-is', without any express or implied warranty. In
 no event will the authors be held liable for any damages arising from the use
 of this software.
 .
 Permission is granted to anyone to use this software for any purpose, including
 commercial applications, and to alter it and redistribute it freely, subject to
 the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not claim
 that you wrote the original software. If you use this software in a product, an
 acknowledgment in the product documentation would be appreciated but is not
 required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source distribution.
